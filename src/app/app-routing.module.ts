import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BookDetailComponent } from './components/book-detail/book-detail.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { HomeComponent } from './components/home/home.component';
import { EditBookComponent } from './components/edit-book/edit-book.component';
import { CategoryComponent } from './components/category/category.component';



const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: 'add-book',
    component: AddBookComponent,
  },
  {
    path: 'book/:id',
    component: BookDetailComponent,
  },
  {
    path: 'edit-book/:id',
    component: EditBookComponent,
  },
  {
    path: 'category',
    component: CategoryComponent,
  },
  { path: '', redirectTo: '/home', pathMatch: 'full' }


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
