import { ClassGetter } from '@angular/compiler/src/output/output_ast';
import { AfterContentInit, Component, DoCheck, OnChanges, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { Category } from 'src/app/models/category';
import { BookService } from 'src/app/services/book.service';
import { CategoryService } from 'src/app/services/category.service';
import Swal from 'sweetalert2';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit{
  constructor( private bookService: BookService, private router: Router, private cateService: CategoryService) { }

  books : Book[]
  categories: Category[]
  p: number = 1;
  

  ngOnInit(): void {
    this.cateService.getCategories().subscribe(res =>{
      this.categories = res._embedded.categories
      console.log(this.categories);
    })

    this.getBooks();
  }


  getBooks(): void {


    this.bookService.getBooks().subscribe((res: any)=>{
      this.books = res._embedded.books;
      console.log(this.books);
    })
  }

  onChange(event: any){
    this.bookService.findBookByCategoryId(event.id).subscribe(res =>{
      console.log(res);
      this.books = res._embedded.books      
    })
    
  }

  onTitleChange(event: any){
    console.log(event.target.value);
    
    this.bookService.findBookByTitle(event.target.value).subscribe(res =>{
      this.books = res._embedded.books  
    })
  }


  goToBookDetail(id: any): void {
    this.router.navigate(['/book', id])
    console.log(id);
  }

  goToAddBook(): void{
    
    this.router.navigate(["/add-book"])
  }

  goToEditBook(id: any): void {
    this.router.navigate(['/edit-book', id])
  }

  


  deleteBook(id: any): void {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.bookService.deleteBook(id).subscribe(() =>
        (this.books = this.books.filter((item) => item.id !== id))
      )
      
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Book delete successfully'
      })
      }
    })  
  }
  

  


}
