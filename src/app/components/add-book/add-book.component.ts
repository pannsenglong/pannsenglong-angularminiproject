import { HttpClient } from '@angular/common/http';
import { AfterContentInit, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgSelectConfig } from '@ng-select/ng-select';
import { Book } from 'src/app/models/book';
import { Category } from 'src/app/models/category';
import { BookService } from 'src/app/services/book.service';
import { CategoryService } from 'src/app/services/category.service';
import Swal from 'sweetalert2';
import { ImageServiceService } from '../../services/image.service';
import { BookDetailComponent } from '../book-detail/book-detail.component';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css'],
})
export class AddBookComponent implements OnInit {
  
  constructor(
    private imageService: ImageServiceService,
    private http: HttpClient,
    private bookService: BookService,
    private route: Router,
    private routeNavigate: ActivatedRoute,
    private cateService: CategoryService,
  ) {
  }

  defaultImage = '../../assets/default-book.png';
  myForm: any;
  selectedFile: any;

  public toggleFlag = false;
  selectedTitleId: number

  tit: any;
  aut: any;
  des: any;
  thum: any;
  imageSrc: any;
  cateId: number;
  selectedCar: number;
  categories: any
 
    cars = [
        { id: 1, name: 'Volvo' },
        { id: 2, name: 'Saab' },
        { id: 3, name: 'Opel' },
        { id: 4, name: 'Audi' },
    ];

  id: any

  book: Book;


  ngOnInit() {
    this.id = this.routeNavigate.snapshot.paramMap.get('id') 

    this.cateService.getCategories().subscribe(res =>{
      console.log(res);
      
      this.categories = res._embedded.categories
      console.log(this.categories);
    }) 

    this.myForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      description: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      author: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
      file: new FormControl('', [Validators.required]),
      category: new FormControl('', [Validators.required]),

    });

  }


  onChange(event: any){
    this.cateId = event.id
    
  }



  onFileChange(event: any) {
    console.log(this.categories);
    
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      this.selectedFile = <File>event.target.files[0];

      console.log(this.selectedFile);

      reader.readAsDataURL(this.selectedFile);
      reader.onload = () => {
        this.imageSrc = reader.result as string;
      };
    }
  }



  submit() {
    this.imageService.postFile(this.selectedFile).subscribe((res) => {
      console.log(res.imageUrl);

      this.thum = res.imageUrl;
      console.log(this.thum);

      this.book = {
        id: '',
        title: this.tit,
        author: this.aut,
        description: this.des,
        thumbnail: this.thum,
        category: { id: this.cateId, title: '' },
      };
      console.log(this.book);
      this.bookService.postBook(this.book).subscribe((res) => {
        console.log(res);
        if(res)
        {
          const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })
          swalWithBootstrapButtons.fire(
            'Successfully!',
            'Your book has been added.',
            'success'
          )
          this.route.navigate(["/home"]);
        }
      });
    });
  }

  get title() {
    return this.myForm.get('title');
  }

  get author() {
    return this.myForm.get('author');
  }

  get description() {
    return this.myForm.get('description');
  }

  get file() {
    return this.myForm.get('file');
  }

  get category(){
    return this.myForm.get('category')
  }

  onTitleChange(event: any) {
    this.tit = event;
  }

  onAuthorChange(event: any) {
    this.aut = event;
  }

  onDescriptionChange(event: any) {
    this.des = event;
  }

}
