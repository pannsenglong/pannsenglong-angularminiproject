import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Category } from 'src/app/models/category';
import { CategoryService } from 'src/app/services/category.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {

  constructor(private cateService: CategoryService) { }

  categories: Category[]
  category: Category
  myForm: any
  isUpdate: boolean
  tit: any
  updateId: any
  p: number = 1;
  

  ngOnInit(): void {
    this.myForm = new FormGroup({
      title: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
      ]),
    })

    this.getCate()
  }


  submit(){
    this.postCate();
  }

  getCate(){
    console.log("run");
    
    return this.cateService.getCategories().subscribe(res =>{
      console.log(res._embedded.categories);
      this.categories = res._embedded.categories
    })
  }

  postCate(){
    this.category = {"id": "", "title": this.tit }
    if(this.isUpdate)
    {
      console.log(this.category);
      this.cateService.updateCategory(this.updateId, this.category).subscribe(res =>{
        if(res){
          const swalWithBootstrapButtons = Swal.mixin({
            customClass: {
              confirmButton: 'btn btn-success',
              cancelButton: 'btn btn-danger'
            },
            buttonsStyling: false
          })
          swalWithBootstrapButtons.fire(
            'Successfully!',
            'Your category has been updated successfuly.',
            'success'
          )
          this.isUpdate = false
           this.tit = ""
        }(this.categories = this.categories.filter((item) => item.id > 0))
        this.getCate()
      })
      return

    }




    
    this.cateService.postCategory(this.category).subscribe((res) => {
      if(res) {
        const swalWithBootstrapButtons = Swal.mixin({
          customClass: {
            confirmButton: 'btn btn-success',
            cancelButton: 'btn btn-danger'
          },
          buttonsStyling: false
        })
        swalWithBootstrapButtons.fire(
          'Successfully!',
          'Your category has been added.',
          'success'
        )
      }
      (this.categories = this.categories.filter((item) => item.id > 0))})
      this.getCate()
  }

  updateCate(id: any, title: string){
    this.tit = title
    this.updateId = id
    this.isUpdate = true  
  }

  deleteCate(id: any){
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.cateService.deleteCategory(id).subscribe(() =>
        (this.categories = this.categories.filter((item) => item.id !== id)))
      
      const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      })
      
      Toast.fire({
        icon: 'success',
        title: 'Category deleted successfully'
      })
      }
    })
    
  }

  onTitleChange(event: any)
  {
    this.tit = event    
  }

  get title(){
    return this.myForm.get("title");
  }
}
