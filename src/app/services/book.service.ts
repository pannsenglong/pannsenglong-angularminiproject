import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Book } from '../models/book';


@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient ) { }

  books: Book[]
  BASE_URL = "http://localhost:8080/api/v1/books/"
  getBooks() {
    return this.http.get<any[]>(this.BASE_URL);
  }

  updateBook(id: any, book: Book){
    return this.http.put<Book>(this.BASE_URL + `${id}`, book);
  }

  postBook(book: Book){
    return this.http.post(this.BASE_URL, book);
  }

  findBookByTitle(title: any){
    return this.http.get<any>(this.BASE_URL + `search/title?title=${title}`);
  }

  getOneBook(id: number) {
    return this.http.get<Book>(this.BASE_URL + `${id}`);
  }


  deleteBook(id: number) {
    return this.http.delete(this.BASE_URL + `${id}`);
  }

  findBookByCategoryId(cateId: any){
    return this.http.get<any>(this.BASE_URL + `search/category?categoryId=${cateId}`)
  }
}
